const mysql = require('mysql');
const {promisify} = require('util');
const _console = require('Console');
const discover = require('./database/discover');
function connect_db(config){
    const pool = mysql.createPool(config);
    pool.getConnection(async (error,connection) => {
        if (error) {
            if (error.code === 'PROTOCOL_CONNECTION_LOST') console.error("DB ERROR: connection was closed.");
            if (error.code === 'ER_CON_COUNT_ERROR') console.error("DB ERROR: to many connections.");
            if (error.code === 'ECONNREFUSED') console.error("DB ERROR: connection was refused.");
        }
        if(connection){
            await connection.release();
            _console.success("Database conected successfully...");
            discover(pool,config.database);
        }
    });
    pool.query = promisify(pool.query)
    return pool;
}

module.exports = connect_db;