async function get_all_data(pool,model){
    var result = await pool.query(`SELECT * FROM ${model}`);
    let items = [];
    
    for (let i = 0; i < result.length; i++) {
        let item = result[i];
        await getting_relations(pool,item)
        items.push(item)
    }
    return items;
}

const getting_relations = async (pool,s_m) => {
    for (var attribute in s_m){
        var key = await attribute.split('_');
        var rel = [];
        if(key.length > 1 && key[0] == 'id'){
            var table = key[1]+'_'+key[2];
            var sql = `SELECT * FROM ${table}s WHERE id = ${s_m[attribute]}`;
            s_m[table] = await pool.query(sql);
            await rel.push({
                s_m,
            })        
        }
    } 
    return rel;
}


module.exports = get_all_data;
