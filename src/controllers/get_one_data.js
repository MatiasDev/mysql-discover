
async function get_one_data(pool,model,id){
    let query = await pool.query(`SELECT * FROM ${model} WHERE id = ${id}`);
    let single_model = query[0];
    for (var attribute in query[0]){
        let key = attribute.split('_');
        if(key.length > 1 && key[0] == 'id'){
            single_model[key[1]+'_'+key[2]] = await pool.query(`SELECT * FROM ${key[1]}_${key[2]}s WHERE id = ${single_model[attribute]}`);
        }
    }
    return single_model;
}

module.exports = get_one_data;