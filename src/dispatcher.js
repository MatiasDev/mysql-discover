/**
 * 
 * @param {Express} app 
 */

 const get_all_data = require('./controllers/get_all_data');
 const store_data = require('./controllers/store_data');
 const update_data = require('./controllers/update_data');
 const delete_data = require('./controllers/delete_data');
 const get_one_data = require('./controllers/get_one_data');

async function dispatcher(app,pool){
    app.get('/:model',async (req,res)=>{
        let model = await get_all_data(pool,req.params.model);
        res.json(model);
    });
    app.get('/:model/:id',async (req,res)=>{
        let model = await get_one_data(pool,req.params.model,req.params.id);
        res.json(model);
    });
    app.post('/:model',async (req,res)=>{
        let model = await store_data(pool,req.params.model,req.body);
        res.json(model);
    });
    app.put('/:model/:id',async (req,res)=>{
        let model = await update_data(pool,req.params.model,req.params.id,req.body);
        res.json(model);
    });
    app.delete('/:model/:id',async (req,res)=>{
        let model = await delete_data(pool,req.params.model,req.params.id);
        res.json(model);
    });

}


module.exports = dispatcher;