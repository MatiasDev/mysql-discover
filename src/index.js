
/**
 * 
 * @param {express.Application} app
 * @param {MySql - Configuration} _dbConfig 
 */
const dispatcher = require('./dispatcher');
const connect_db = require('./db');


async function run (Database_Configuration,express_application){
    const pool = await connect_db(Database_Configuration);
    dispatcher(express_application,pool);
}

module.exports = {run};

