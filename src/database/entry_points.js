const _console = require('Console');
async function generate_entry_points(pool,models,relations){
    _console.warn('-- Models --')
    models.forEach(model => {
        _console.success(`***** ${model.toUpperCase()} *****`);
        _console.debug(`➾ GET:     /${model}/              ➾ CREATED ✔`);
        _console.debug(`➾ GET:     /${model}/:id           ➾ CREATED ✔`);
        _console.debug(`➾ POST:    /${model}               ➾ CREATED ✔`);
        _console.debug(`➾ PUT:     /${model}/:id           ➾ CREATED ✔`);
        _console.debug(`➾ DELETE:  /${model}/:id           ➾ CREATED ✔`);
    });
    _console.warn('-- Relations --')
    relations.forEach(relation => {
        _console.success(`***** ${relation.toUpperCase()} *****`);
        _console.debug(`➾ GET:     /${relation}/              ➾ CREATED ✔`);
        _console.debug(`➾ GET:     /${relation}/:id           ➾ CREATED ✔`);
        _console.debug(`➾ POST:    /${relation}               ➾ CREATED ✔`);
        _console.debug(`➾ PUT:     /${relation}/:id           ➾ CREATED ✔`);
        _console.debug(`➾ DELETE:  /${relation}/:id           ➾ CREATED ✔`);
    });
}

module.exports = generate_entry_points;